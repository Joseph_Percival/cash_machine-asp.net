﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CashMachine.Controllers
{
    public class HomeController : Controller
    {
        //GET /home/index
        public ActionResult Index()
        {
            return View();
        }
        //Get/home/about - this is called routing
        public ActionResult About()
        {
            ViewBag.TheMessage = "Need help? Send us a message.";

            return View();
        }
       
        public ActionResult Contact()
        {
            //View bag is a dynamic object which allows data to be passed back and forth that doesnt
            //belong in a model because it isnt real application data
            ViewBag.TheMessage = "Need help? Send us a message";

            return View();
        }
        [HttpPost]
        public ActionResult Contact(string message)
        {
            // TODO :Send message to HQ
            ViewBag.TheMessage = "Thanks we got your message!";
            return View();
        }
        public ActionResult Serial(string letterCase)
        {
            var serial = "ASP.NET,MVC5-CM1";
            if (letterCase == "lower")
            { 
                return Content(serial.ToLower());
            }
            //return Json(new { name = "serial", value = serial },
             //   JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index");    
            }
        }
    }
